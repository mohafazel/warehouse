package com.ikea.assignment.warehouse.config;


import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.ikea.assignment.warehouse.model.Article;
import com.ikea.assignment.warehouse.model.Product;
import com.ikea.assignment.warehouse.repository.ArticleRepository;
import com.ikea.assignment.warehouse.repository.ComponentRepository;
import com.ikea.assignment.warehouse.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

@Component
public class InitializationConfig {

    private static final String ARTICLES_PATH = "/file/inventory.json";
    private static final String PRODUCTS_PATH = "/file/products.json";



    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ComponentRepository componentRepository;

    @Autowired
    private ArticleRepository articleRepository;

    @PostConstruct
    public void initDB() {
        List<Article> articles = getArticles(getClass().getResourceAsStream(ARTICLES_PATH));
        storeArticles(articles);
        List<Product> products = getProducts(getClass().getResourceAsStream(PRODUCTS_PATH));
        storeProducts(products);
    }

    List<Product> getProducts(InputStream inputStream) {
        List<Product> products = new ArrayList<>();
        try {
            ObjectMapper mapper = getProductsMapper();
            InputStreamReader stream = new InputStreamReader(inputStream);
            JsonNode root = mapper.readTree(stream);
            products = mapper.readValue(
                    root.path("products").toString(),
                    new TypeReference<List<Product>>() {}
            );
        } catch (IOException e) {
            e.printStackTrace();
        }
        return products;
    }

    private ObjectMapper getProductsMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        SimpleModule module = new SimpleModule(
                "ProductDeserializer",
                new Version(1, 0, 0, null, null, null)
        );
        module.addDeserializer(Product.class, new ProductDeserializer());
        objectMapper.registerModule(module);
        return objectMapper;
    }

    List<Article> getArticles(InputStream inputStream) {
        List<Article> articles = new ArrayList<>();
        try {
            ObjectMapper mapper = getArticlesMapper();
            InputStreamReader stream = new InputStreamReader(inputStream);
            JsonNode root = mapper.readTree(stream);
            articles = mapper.readValue(
                    root.path("inventory").toString(),
                    new TypeReference<List<Article>>() {}
            );
        } catch (IOException e) {
            e.printStackTrace();
        }
        return articles;
    }

    private ObjectMapper getArticlesMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        SimpleModule module = new SimpleModule(
                "ArticleDeserializer",
                new Version(1, 0, 0, null, null, null)
        );
        module.addDeserializer(Article.class, new ArticleDeserializer());
        objectMapper.registerModule(module);
        return objectMapper;
    }

    private void storeArticles(List<Article> articles) {
        articleRepository.saveAll(articles);
    }

    private void storeProducts(List<Product> products) {
        productRepository.saveAll(products);
        products.forEach( product -> {
            product.getComponents().forEach( component -> {
                component.setProduct(product);
                componentRepository.save(component);
            });
        });
    }
}
