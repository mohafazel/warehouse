package com.ikea.assignment.warehouse.config;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.ikea.assignment.warehouse.model.Article;
import com.ikea.assignment.warehouse.model.Component;
import com.ikea.assignment.warehouse.model.Product;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ProductDeserializer extends StdDeserializer<Product> {

    public ProductDeserializer() { this(null); }

    public ProductDeserializer(Class<?> vc) { super(vc); }

    @Override
    public Product deserialize(
            JsonParser jsonParser,
            DeserializationContext deserializationContext) throws IOException {
        Product product = new Product();
        ObjectCodec codec = jsonParser.getCodec();
        JsonNode node = codec.readTree(jsonParser);

        JsonNode nameNode = node.get("name");
        String name = nameNode.asText();
        product.setName(name);
        JsonNode componentNode = node.get("contain_articles");
        ArrayNode componentArray = (ArrayNode) componentNode;
        List<Component> components = new ArrayList<>();
        for (JsonNode componentJson: componentArray) {
            Component component = new Component();
            component.setArticle(new Article(componentJson.get("art_id").asLong()));
            component.setAmount(componentJson.get("amount_of").asInt());
            components.add(component);
        }
        product.setComponents(components);
        return product;
    }
}
