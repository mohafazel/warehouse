package com.ikea.assignment.warehouse.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Autowired
    private Environment env;

    /**
     * Identifies the configuration of Swagger in order to have a testable user interface of endpoints.
     */
    @Bean
    public Docket api(){
        String host = env.getProperty("swagger.host");
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.ikea.assignment.warehouse.resource"))
                .paths(PathSelectors.any())
                .build()
                .host(host)
                .apiInfo(apiInfo());
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder().title("IKEA Warehouse")
                .description("An application which is implemented as part of the hiring process assignment for IKEA.")
                .build();
    }
}
