package com.ikea.assignment.warehouse.config;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.ikea.assignment.warehouse.model.Article;

import java.io.IOException;

public class ArticleDeserializer extends StdDeserializer<Article> {

    public ArticleDeserializer() {
        this(null);
    }

    public ArticleDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public Article deserialize(
            JsonParser jsonParser,
            DeserializationContext deserializationContext) throws IOException {
        Article article = new Article();
        ObjectCodec codec = jsonParser.getCodec();
        JsonNode node = codec.readTree(jsonParser);

        JsonNode idNode = node.get("art_id");
        Long id = idNode.asLong();
        article.setId(id);
        JsonNode nameNode = node.get("name");
        String name = nameNode.asText();
        article.setName(name);
        JsonNode stockNode = node.get("stock");
        Integer stock = stockNode.asInt();
        article.setStock(stock);
        return article;
    }
}
