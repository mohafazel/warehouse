package com.ikea.assignment.warehouse.service;

import com.ikea.assignment.warehouse.model.Product;
import com.ikea.assignment.warehouse.model.ProductDTO;
import com.ikea.assignment.warehouse.model.SellResponse;
import com.ikea.assignment.warehouse.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService {

    @Autowired
    private ProductRepository productRepository;

    public List<ProductDTO> getAll() {
        return productRepository.getAll();
    }

    public synchronized SellResponse sellProduct(Long productId) {
        Optional<ProductDTO> availableProduct = Optional.ofNullable(productRepository.getAvailableProduct(productId));
        if (availableProduct.isEmpty()) {
            return new SellResponse(true, "There is no product with that ID");
        } else if (availableProduct.get().getAvailable() == 0) {
            return new SellResponse(true, "This product is not currently available in stock");
        }
        productRepository.sellProduct(productId);
        return new SellResponse(false);
    }
}
