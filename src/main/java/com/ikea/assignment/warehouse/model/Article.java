package com.ikea.assignment.warehouse.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "articles")
public class Article {

    @Id
    private Long id;
    private String name;
    private Integer stock;

    public Article() {}
    public Article(Long id) {
        this.id = id;
    }
}
