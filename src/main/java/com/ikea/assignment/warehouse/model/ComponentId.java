package com.ikea.assignment.warehouse.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Data
@Embeddable
public class ComponentId implements Serializable {

    @Column(name = "article_id")
    private Long articleId;
    @Column(name = "product_id")
    private Long productId;

    public ComponentId() {}

    public ComponentId(Long articleId, Long productId) {
        this.articleId = articleId;
        this.productId = productId;
    }
}
