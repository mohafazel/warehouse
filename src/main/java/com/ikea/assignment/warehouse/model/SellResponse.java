package com.ikea.assignment.warehouse.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class SellResponse {

    private boolean isError;
    private String errorDescription;

    public SellResponse(boolean isError) {
        this.isError = isError;
    }
}
