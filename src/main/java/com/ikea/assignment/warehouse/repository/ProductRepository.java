package com.ikea.assignment.warehouse.repository;

import com.ikea.assignment.warehouse.model.Product;
import com.ikea.assignment.warehouse.model.ProductDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

    @Query("SELECT new com.ikea.assignment.warehouse.model.ProductDTO(p.id, p.name, p.price, MIN(a.stock/c.amount)) " +
            "FROM Product p " +
            "JOIN Component c ON p.id = c.product.id " +
            "JOIN Article a ON c.article.id = a.id " +
            "GROUP BY p.name")
    List<ProductDTO> getAll();

    @Query("SELECT new com.ikea.assignment.warehouse.model.ProductDTO(p.id, p.name, p.price, MIN(a.stock/c.amount)) " +
            "FROM Product p " +
            "JOIN Component c ON p.id = c.product.id " +
            "JOIN Article a ON c.article.id = a.id " +
            "WHERE p.id = :productId " +
            "GROUP BY p.name")
    ProductDTO getAvailableProduct(@Param("productId") Long productId);

    @Transactional
    @Modifying
    @Query(value = "UPDATE articles a " +
            "SET a.stock = a.stock - (SELECT c.amount FROM components c WHERE c.article_id = a.id AND c.product_id = :productId) " +
            "WHERE EXISTS (SELECT * FROM components c WHERE c.article_id = a.id AND c.product_id = :productId)",
            nativeQuery = true
    )
    void sellProduct(@Param("productId") Long productId);
}
