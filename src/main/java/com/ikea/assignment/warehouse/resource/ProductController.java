package com.ikea.assignment.warehouse.resource;

import com.ikea.assignment.warehouse.model.Product;
import com.ikea.assignment.warehouse.model.ProductDTO;
import com.ikea.assignment.warehouse.model.SellResponse;
import com.ikea.assignment.warehouse.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/products")
public class ProductController {

    @Autowired
    private ProductService productService;

    @GetMapping
    public List<ProductDTO> getAllProducts() {
        return productService.getAll();
    }

    @DeleteMapping("/sell/{productId}")
    public ResponseEntity sellProduct(@PathVariable Long productId) {
        SellResponse response = productService.sellProduct(productId);
        if (response.isError()) return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response.getErrorDescription());
        return ResponseEntity.ok("Product successfully sold!");
    }
}
