package com.ikea.assignment.warehouse.resource;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ikea.assignment.warehouse.model.ProductDTO;
import com.ikea.assignment.warehouse.model.SellResponse;
import com.ikea.assignment.warehouse.service.ProductService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class ProductControllerTest {

    private static final String ALL_PRODUCTS = "[{\"id\":1,\"name\":\"Chair\",\"price\":49.95,\"available\":3},{\"id\":2,\"name\":\"Table\",\"price\":199.95,\"available\":2}]";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ProductService productService;

    ObjectMapper mapper = new ObjectMapper();

    @Test
    void getAllProducts() throws Exception {
        List<ProductDTO> products = Arrays.asList(
                new ProductDTO(1L, "Chair", new BigDecimal("49.95"), 3),
                new ProductDTO(2L, "Table", new BigDecimal("199.95"), 2)
        );
        when(productService.getAll()).thenReturn(products);
        String json = mockMvc.perform(get("/api/products"))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andReturn().getResponse().getContentAsString();

        assertEquals(ALL_PRODUCTS, json);
    }

    @Test
    void sellProductHappyFlow() throws Exception {
        when(productService.sellProduct(1L)).thenReturn(new SellResponse(false));
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/products/sell/1"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    void sellProductWhenInvalidProductId() throws Exception {
        when(productService.sellProduct(1L)).thenReturn(
                new SellResponse(true, "There is no product with that ID")
        );
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/products/sell/1"))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }
}