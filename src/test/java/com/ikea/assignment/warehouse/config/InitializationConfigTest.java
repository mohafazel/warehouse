package com.ikea.assignment.warehouse.config;

import com.ikea.assignment.warehouse.model.Article;
import com.ikea.assignment.warehouse.model.Product;
import com.ikea.assignment.warehouse.util.ArticleMockBuilder;
import com.ikea.assignment.warehouse.util.ComponentMockBuilder;
import com.ikea.assignment.warehouse.util.ProductMockBuilder;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class InitializationConfigTest {

    private static final String ARTICLES_PATH = "/file/inventory.json";
    private static final String PRODUCTS_PATH = "/file/products.json";

    @Autowired
    InitializationConfig initializationConfig;

    @Test
    void getProducts() {
        InputStream inputStream = getClass().getResourceAsStream(PRODUCTS_PATH);
        List<Product> products = initializationConfig.getProducts(inputStream);
        assertEquals(2, products.size());
        assertEquals(3, products.get(0).getComponents().size());
        assertEquals("Dining Chair", products.get(0).getName());
        assertEquals(3, products.get(1).getComponents().size());
        assertEquals("Dinning Table", products.get(1).getName());
    }

    @Test
    void getArticles() {
        InputStream inputStream = getClass().getResourceAsStream(ARTICLES_PATH);
        List<Article> articles = initializationConfig.getArticles(inputStream);
        assertEquals(4, articles.size());
        assertEquals("leg", articles.get(0).getName());
        assertEquals(12, articles.get(0).getStock());
        assertEquals("screw", articles.get(1).getName());
        assertEquals(17, articles.get(1).getStock());
        assertEquals("seat", articles.get(2).getName());
        assertEquals(2, articles.get(2).getStock());
        assertEquals("table top", articles.get(3).getName());
        assertEquals(1, articles.get(3).getStock());
    }

    private List<Product> getMockProducts() {
        return Arrays.asList(
                new ProductMockBuilder().setName("Dining Chair")
                .setComponents(Arrays.asList(
                        new ComponentMockBuilder().setArticle(
                                new ArticleMockBuilder().setId(1L).build()
                        ).setAmount(4).build(),
                        new ComponentMockBuilder().setArticle(
                                new ArticleMockBuilder().setId(2L).build()
                        ).setAmount(8).build(),
                        new ComponentMockBuilder().setArticle(
                                new ArticleMockBuilder().setId(3L).build()
                        ).setAmount(1).build()
                )).build(),
                new ProductMockBuilder().setName("Dining Table")
                        .setComponents(Arrays.asList(
                                new ComponentMockBuilder().setArticle(
                                        new ArticleMockBuilder().setId(1L).build()
                                ).setAmount(4).build(),
                                new ComponentMockBuilder().setArticle(
                                        new ArticleMockBuilder().setId(2L).build()
                                ).setAmount(8).build(),
                                new ComponentMockBuilder().setArticle(
                                        new ArticleMockBuilder().setId(4L).build()
                                ).setAmount(1).build()
                        )).build()
        );
    }
}