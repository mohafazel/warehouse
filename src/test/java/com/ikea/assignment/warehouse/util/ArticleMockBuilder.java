package com.ikea.assignment.warehouse.util;

import com.ikea.assignment.warehouse.model.Article;

public class ArticleMockBuilder {

    private Article article = new Article();

    public Article build() { return this.article; }

    public ArticleMockBuilder setId(Long id) {
        this.article.setId(id);
        return this;
    }

    public ArticleMockBuilder setName(String name) {
        this.article.setName(name);
        return this;
    }

    public ArticleMockBuilder setStock(Integer stock) {
        this.article.setStock(stock);
        return this;
    }
}
