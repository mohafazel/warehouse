package com.ikea.assignment.warehouse.util;

import com.ikea.assignment.warehouse.model.Component;
import com.ikea.assignment.warehouse.model.Product;

import java.math.BigDecimal;
import java.util.List;

public class ProductMockBuilder {

    private Product product = new Product();

    public Product build() { return this.product; }

    public ProductMockBuilder setId(Long id) {
        this.product.setId(id);
        return this;
    }

    public ProductMockBuilder setName(String name) {
        this.product.setName(name);
        return this;
    }

    public ProductMockBuilder setPrice(BigDecimal price) {
        this.product.setPrice(price);
        return this;
    }

    public ProductMockBuilder setComponents(List<Component> components) {
        this.product.setComponents(components);
        return this;
    }
}
