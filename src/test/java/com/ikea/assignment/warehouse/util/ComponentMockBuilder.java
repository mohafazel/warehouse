package com.ikea.assignment.warehouse.util;

import com.ikea.assignment.warehouse.model.Article;
import com.ikea.assignment.warehouse.model.Component;
import com.ikea.assignment.warehouse.model.Product;

public class ComponentMockBuilder {

    private Component component = new Component();

    public Component build() { return this.component; }

    public ComponentMockBuilder setArticle(Article article) {
        this.component.setArticle(article);
        return this;
    }

    public ComponentMockBuilder setProduct(Product product) {
        this.component.setProduct(product);
        return this;
    }

    public ComponentMockBuilder setAmount(Integer amount) {
        this.component.setAmount(amount);
        return this;
    }
}
