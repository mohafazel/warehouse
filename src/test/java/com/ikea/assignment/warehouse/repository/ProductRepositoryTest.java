package com.ikea.assignment.warehouse.repository;

import com.ikea.assignment.warehouse.model.Article;
import com.ikea.assignment.warehouse.model.Component;
import com.ikea.assignment.warehouse.model.Product;
import com.ikea.assignment.warehouse.model.ProductDTO;
import com.ikea.assignment.warehouse.util.ArticleMockBuilder;
import com.ikea.assignment.warehouse.util.ComponentMockBuilder;
import com.ikea.assignment.warehouse.util.ProductMockBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@DataJpaTest
class ProductRepositoryTest {

    @Autowired
    TestEntityManager entityManager;

    @Autowired
    ProductRepository productRepository;

    @BeforeEach
    private void persistMockEntityForQuery() {
        Article leg = new ArticleMockBuilder().setId(1L).setName("leg").setStock(12).build();
        Article screw = new ArticleMockBuilder().setId(2L).setName("screw").setStock(17).build();
        Article seat = new ArticleMockBuilder().setId(3L).setName("seat").setStock(2).build();
        Component chairLeg = new ComponentMockBuilder().setArticle(leg).setAmount(4).build();
        Component chairScrew = new ComponentMockBuilder().setArticle(screw).setAmount(8).build();
        Component chairSeat =new ComponentMockBuilder().setArticle(seat).setAmount(1).build();
        Product chair = new ProductMockBuilder().setName("Dining Chair").setPrice(new BigDecimal("29.95"))
                .setComponents(Arrays.asList(chairLeg, chairScrew, chairSeat)).build();
        entityManager.persist(leg);
        entityManager.persist(screw);
        entityManager.persist(seat);
        entityManager.persist(chair);
        chair.getComponents().forEach( component -> {
            component.setProduct(chair);
            entityManager.persist(component);
        });
    }

    @Test
    void getAll() {
        List<ProductDTO> products = productRepository.getAll();
        assertEquals(1, products.size());
        assertEquals(2, products.get(0).getAvailable());
    }

    @Test
    void getAvailableProduct() {
        ProductDTO productDTO = productRepository.getAll().get(0);
        ProductDTO product = productRepository.getAvailableProduct(productDTO.getId());
        assertEquals(2, product.getAvailable());
        assertEquals(new BigDecimal("29.95"), product.getPrice());
    }

    @Test
    void sellProduct() {
        ProductDTO product = productRepository.getAll().get(0);
        productRepository.sellProduct(product.getId());
        ProductDTO availableProduct = productRepository.getAvailableProduct(product.getId());
        assertEquals(1, availableProduct.getAvailable());
        assertEquals(new BigDecimal("29.95"), availableProduct.getPrice());
    }
}