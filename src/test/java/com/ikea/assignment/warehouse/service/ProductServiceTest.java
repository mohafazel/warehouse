package com.ikea.assignment.warehouse.service;

import com.ikea.assignment.warehouse.model.ProductDTO;
import com.ikea.assignment.warehouse.model.SellResponse;
import com.ikea.assignment.warehouse.repository.ProductRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.math.BigDecimal;

import static org.mockito.Mockito.when;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class ProductServiceTest {

    @MockBean
    private ProductRepository productRepository;

    @Autowired
    private ProductService productService;

    @Test
    void sellProductHappyFlow() {
        when(productRepository.getAvailableProduct(5L)).thenReturn(
                new ProductDTO(5L, "Chair", new BigDecimal("109.95"), 2)
        );
        SellResponse response = productService.sellProduct(5L);

        assertFalse(response.isError());
    }

    @Test
    void sellProductWhenInvalidProductId() {
        when(productRepository.getAvailableProduct(5L)).thenReturn(null);
        SellResponse response = productService.sellProduct(5L);

        assertTrue(response.isError());
        assertEquals("There is no product with that ID", response.getErrorDescription());
    }

    @Test
    void sellProductWhenUnavailableProduct() {
        when(productRepository.getAvailableProduct(5L)).thenReturn(
                new ProductDTO(5L, "Chair", new BigDecimal("109.95"), 0)
        );
        SellResponse response = productService.sellProduct(5L);

        assertTrue(response.isError());
        assertEquals("This product is not currently available in stock", response.getErrorDescription());
    }
}