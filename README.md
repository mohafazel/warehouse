# IKEA-warehouse

This application is implemented as part of application process for IKEA. The assignment is to implement a warehouse software. This software holds articles, and the articles contain an identification number, a name and available stock. The articles load into the software from a file, see the `inventory.json` file in resource folder.

The warehouse software also have products, products are made of different articles. Products have a name, price and a list of articles of which they are made from with a quantity. The products loaded from a file, see `products.json` file in resource folder.

The warehouse has the following functionality;
* Get all products and quantity of each that is an available with the current inventory
* Remove(Sell) a product and update the inventory accordingly

## Build
You can simply build the project using `mvn clean install`

## Run
Also, the project can run using `mvn spring-boot:run`

After run the project all the endpoints can be find in the url below:
`http://localhost:8080/swagger-ui.html`

If you need to look into the h2 database which is used in this project as a cache, it is available in the url below:
`http://localhost:8080/console`

Use `jdbc:h2:mem:warehouse` as JDBC URL and `sa` as User Name to connect the h2 database.  